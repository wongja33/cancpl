.. CanCPL documentation master file

Welcome to CanCPL's documentation!
==================================

Contents:

.. toctree::
  :maxdepth: 2

  apiref
  communication
  workflow


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

