Communicating between components
===================================
CanESM5 runs in a multiple-program, multiple-domain paradigm where each component (atmosphere, ocean, and coupler) run
their own executables and communicate using MPI. All MPI tasks exist in the same ``MPI_COMM_WORLD``. CanCPL itself is
run on a single MPI task (and has no further parallelization within it). This page details how fields are sent
between components and the transformations that occur.

Coupler API
~~~~~~~~~~~
All three components compile ``com_cpl.F90`` containing routines that initialize the MPI communicators on every task,
create MPI communicators, define the interfaces to send and receive data via MPI (see the API reference for a full
description), and setup the list of variables that will be passed through the coupler. The coupler API are only accessed
in a handful of routines in each component:

AGCM

- ``gcm18.F``
- ``mpi_getcpl2.F``
- ``mpi_putggb2.F``

Ocean

- ``cpl_cancpl.F90``

Organization of MPI communications
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Every MPI task exists in the same general ``MPI_COMM_WORLD``, but are further organized into their own labeled
`MPI_COMM` (referred to in MPI as a 'colored communicator') based on the component model (i.e. ocean, coupler, and
atmosphere). The communicators for each component are created during their respective initializations via a call to
``define_group``. Every MPI task calling this routine broadcasts its own 'color' and rank within the ``MPI_COMM_WORLD``
to every other task as well. The ranks associated with each label are then calculated and stored. The task with the
lowest MPI rank in each communicator is desginated the master task.

In general, the transfer of fields between communicators is handled only by the master tasks. These fields comprise the
entire global array and not just the subdomain (in the ocean) or latitude bands (in the atmosphere) associated with an
individual task. After receiving the field, the master task scatters the global array to all other tasks within the
communicator.

The following describes the communication pathway using sea surface temperature as an example:

1. The master ``ocn`` task constructs the global SST array from the subdomain of every other ``ocn`` task.
2. The master ``ocn`` task sends SST to the main (and only) ``cpl`` task.
3. The ``cpl`` task remaps SST from the ocean grid to the atmospheric grid
4. The ``cpl`` task sends the global SST array to the master ``atm`` task
5. The master ``atm`` task scatters the global SST array to every other ``atm`` task.